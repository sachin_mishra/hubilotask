//
//  APIManager.swift
//  HubiloTask
//
//  Created by SachchidaNand Mishra on 28/10/20.
//

import Foundation
import Alamofire

class APIManager {
    static let dataParsingError = "Error : Data parsing error!"
    static func movieListAPI(completion: @escaping (MovieListResponse) -> Void, failure: @escaping () -> Void) {
        Spinner.show()
        let request = AF.request(K.Movie.listAPIURL)
        request.responseData { (response) in
            Spinner.hide()
            guard let data = response.data else { failure(); return; }
            let decoder = JSONDecoder()
            if let apiResponseData = try? decoder.decode(MovieListResponse.self, from: data) {
                print(apiResponseData)
                if apiResponseData.status == nil {
                    if let message = apiResponseData.message {
                        Spinner.showSuccessMessage(message)
                    }
                    completion(apiResponseData)
                } else {
                    if let message = apiResponseData.message {
                        Spinner.showErrorMessage(message)
                    }
                    failure()
                }
            } else {
                if let errorStr = String(data: response.data!, encoding: String.Encoding.utf8) {
                    Spinner.showErrorMessage(errorStr)
                } else {
                    Spinner.showErrorMessage(dataParsingError)
                }
                failure()
            }
        }
    }
}
