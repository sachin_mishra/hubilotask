//
//  Constants.swift
//  HubiloTask
//
//  Created by SachchidaNand Mishra on 28/10/20.
//

import Foundation


struct K {
    struct Movie {
        private static let baseAPIURL = "https://api.themoviedb.org/3"
        private static let apiKEY = "4524dbe1cb31c1afbf85b85e0f8963c2"
        private static let extraParams = "?api_key=" + K.Movie.apiKEY + "&language=en-US&page=1"
        static let baseImageURL = "https://image.tmdb.org/t/p/w500"
        static let listAPIURL = baseAPIURL + "/movie/now_playing" + extraParams
        static let searchAPIURL = baseAPIURL + "/search/movie" + extraParams
    }
    //
    struct staticString {
        static let empty = ""
    }
    //
    struct SegueIdentifier {
        static let movieDetailSegue = "movieDetailSegue"
    }
}
