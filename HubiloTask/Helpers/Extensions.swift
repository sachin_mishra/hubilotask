//
//  Extensions.swift
//  HubiloTask
//
//  Created by SachchidaNand Mishra on 28/10/20.
//

import Foundation
import UIKit
import SDWebImage


extension UIImageView {
    func addImage(withURLString urlString:String? = K.staticString.empty, image: UIImage? = nil) {
        if image != nil {
            self.image = image
        } else {
            self.sd_setImage(with: URL(string: K.Movie.baseImageURL + urlString!), placeholderImage: #imageLiteral(resourceName: "blog"))
        }
    }
}
