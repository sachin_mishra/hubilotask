//
//  Spinner.swift
//  HubiloTask
//
//  Created by SachchidaNand Mishra on 28/10/20.
//

import Foundation
import SVProgressHUD

class Spinner {
    static func show() {
        SVProgressHUD.show()
    }
    
    static func hide() {
        SVProgressHUD.dismiss()
    }
    
    static func showSuccessMessage(_ message:String) {
        print(message)
        SVProgressHUD.showSuccess(withStatus: message)
        let delayTime = DispatchTime.now() + 3.0
        DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
            Spinner.hide()
        })
    }
    
    static func showErrorMessage(_ message:String) {
        print(message)
        SVProgressHUD.showError(withStatus: message)
        let delayTime = DispatchTime.now() + 3.0
        DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
            Spinner.hide()
        })
    }
}
