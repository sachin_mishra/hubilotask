//
//  DetailViewController.swift
//  HubiloTask
//
//  Created by SachchidaNand Mishra on 26/10/20.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var movieDetailView: MovieDetailView!
    var movie : Movie?
    override func viewDidLoad() {
        super.viewDidLoad()

        movieDetailView.movie = movie
        movieDetailView.setUI()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
