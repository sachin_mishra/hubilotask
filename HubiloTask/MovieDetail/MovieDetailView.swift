//
//  MovieDetailView.swift
//  HubiloTask
//
//  Created by SachchidaNand Mishra on 28/10/20.
//

import UIKit

class MovieDetailView: NSObject {
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var releaseStatusLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var votingLabel: UILabel!
    //
    var movie : Movie?
    
    func setUI() {
        self.posterImage.addImage(withURLString: movie?.posterURLString)
        self.nameLabel.text = movie?.name
        self.overviewLabel.text = movie?.overView
        self.releaseStatusLabel.text = getReleaseStatus()
        self.releaseDateLabel.text = movie?.releaseDate
        self.votingLabel.text = "\(movie?.voting ?? 0)"
    }
    
    private func getReleaseStatus() -> String {
        let isoDate = movie?.releaseDate

        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let releaseDate = dateFormatter.date(from:isoDate!)!
        let currentDate = Date()
        var releaseStatus = ""
        if currentDate > releaseDate {
            releaseStatus = "Released"
        } else {
            releaseStatus = "Upcoming"
        }
        
        return releaseStatus
    }
}
