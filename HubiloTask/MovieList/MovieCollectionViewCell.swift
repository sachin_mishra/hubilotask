//
//  MovieCollectionViewCell.swift
//  HubiloTask
//
//  Created by SachchidaNand Mishra on 28/10/20.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
}

extension MovieCollectionViewCell {
    func configure(data movie: Movie?) {
        self.posterImageView.addImage(withURLString: movie?.posterURLString)
        self.nameLabel.text = movie?.name
        let stringArray = movie?.genreIDs!.map { String($0) }
        self.genreLabel.text = stringArray?.joined(separator: ", ")
    }
}
