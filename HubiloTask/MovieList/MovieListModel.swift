//
//  MovieListModel.swift
//  HubiloTask
//
//  Created by SachchidaNand Mishra on 28/10/20.
//

import Foundation

struct MovieListResponse : Codable {
    let movies : [Movie]?
    let status : Bool?
    let message : String?
    
    enum CodingKeys : String, CodingKey {
        case movies = "results"
        case status = "success"
        case message = "status_message"
    }
}

struct Movie : Codable {
    let id : Int?
    let posterURLString : String?
    let name : String?
    let overView : String?
    let releaseDate : String?
    let genreIDs : [Int]?
    let voting : Int?
    
    enum CodingKeys : String, CodingKey {
        case id
        case posterURLString = "poster_path"
        case name = "title"
        case overView = "overview"
        case releaseDate = "release_date"
        case genreIDs = "genre_ids"
        case voting = "vote_count"
    }
}
