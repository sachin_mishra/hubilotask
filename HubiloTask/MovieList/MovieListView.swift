//
//  MovieListView.swift
//  HubiloTask
//
//  Created by SachchidaNand Mishra on 27/10/20.
//

import UIKit

class MovieListView : NSObject {
    @IBOutlet weak var movieListVC: MovieListViewController!
    @IBOutlet weak var collectionView: UICollectionView!
    //
    var movieData : [Movie]?
    var mainMovieData : [Movie]?
    var movieSearchData : [Movie] = []
    var selectedIndex : NSInteger = 0
    var currentSearchText : String?
    var searchController : UISearchController?
    var isFiltering: Bool {
        if (searchController?.isActive == true) && (searchController?.searchBar.text?.isEmpty == false) {
            return true
        } else {
            return false
        }
    }
}

extension MovieListView : UISearchResultsUpdating, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        print("Searching with: " + (searchController.searchBar.text ?? ""))
        //let searchText = (searchController.searchBar.text ?? "")
        //self.currentSearchText = searchText
        //search()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.currentSearchText = K.staticString.empty
        self.searchController?.isActive = false
        search()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.currentSearchText = searchBar.text
        search()
    }
    
    func search() {
        guard let movies = mainMovieData else { return }
        let filteredData = movies.filter { (movie: Movie) in
            return (movie.name?.lowercased().contains((self.currentSearchText?.lowercased())!))!
          }
        print(filteredData)
        movieSearchData = filteredData
        collectionView.reloadData()
    }
}

@available(iOS 11.0, *)
extension MovieListView {
    func setUI(data:[Movie]?) {
        movieData = data
        mainMovieData = data
        configureSearchController()
        collectionView.reloadData()
    }
    
    func configureSearchController() {
        searchController = UISearchController(searchResultsController: nil)
        searchController?.searchResultsUpdater = self
        searchController?.searchBar.delegate = self
        searchController?.obscuresBackgroundDuringPresentation = false
        searchController?.searchBar.placeholder = "Search Movie"
        self.movieListVC.navigationItem.searchController = searchController
        //movieListVC.navigationItem.titleView = searchController.searchBar;
        self.movieListVC.definesPresentationContext = true
    }
}

extension MovieListView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isFiltering {
            movieData = movieSearchData
        } else {
            movieData = mainMovieData
        }
        return movieData?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath) as! MovieCollectionViewCell
        cell.accessibilityIdentifier = nil
        //
        let movie = self.movieData?[indexPath.row]
        cell.configure(data: movie)
        
        return cell
    }
}

extension MovieListView: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Row : \(indexPath.item + 1)\nSection : \(indexPath.section + 1)")
        selectedIndex = indexPath.row
        self.movieListVC.performSegue(withIdentifier: K.SegueIdentifier.movieDetailSegue, sender: nil)
    }
}

extension MovieListViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == K.SegueIdentifier.movieDetailSegue {
            let detailVC = segue.destination as! DetailViewController
            detailVC.movie = movieListView.movieData?[movieListView.selectedIndex]
        }
    }
}
