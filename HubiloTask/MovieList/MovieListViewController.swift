//
//  ViewController.swift
//  HubiloTask
//
//  Created by SachchidaNand Mishra on 26/10/20.
//

import UIKit

class MovieListViewController: UIViewController {
    @IBOutlet weak var movieListView: MovieListView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Movies"
        
        APIManager.movieListAPI { [self] (response) in
            print(response)
            guard let movies = response.movies else { return }
            if #available(iOS 11.0, *) {
                movieListView.setUI(data: movies)
            } else {
                // Fallback on earlier versions
            }
        } failure: {
            print("error")
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        movieListView.collectionView.reloadData()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        movieListView.collectionView.reloadData()
        if UIDevice.current.orientation.isLandscape {
            print("landscape")
        } else {
            print("portrait")
        }
    }

    @IBAction func searchAction(_ sender: UIBarButtonItem) {
        movieListView.searchController?.isActive = true
        movieListView.searchController?.searchBar.becomeFirstResponder()
    }
    
}

